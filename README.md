# ScantJS
## a scant javascript library
### adjective, scanter, scantest.

1. barely sufficient in amount or quantity; not abundant; almost inadequate:
to do scant justice.

2. limited; meager; not large:
a scant amount.

3. barely amounting to as much as indicated:
a scant two hours; a scant cupful.

## What is ScantJS

ScantJS is a simple javascript file/library that brings some common functionality that is commonly used from different libraries like jQuery.  It has only the bare essentials.  

It includes functions and methods for DOM querying, managing events, manipulation, and some localStorage event firing.  All methods are available natively, the purpose of the code is the cut down the size of regularly used functions and methods.

I'm sure it will continue to grow, but for now it's super simple.

## Prepare

`npm install -g mocha`

`npm install`

`mocha`
