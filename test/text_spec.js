'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: false});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('$$().text', function () {
		it('should get the correct text', function* () {
			let text = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('h1').text;
				});
			expect(text).to.equal('ScantJS');
		});

		it('should set the text correctly', function* () {
			let text = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var h1 = $$('h1');
					h1.text = 'ScantJS Updated';
					return $$('h1').text;
				});
			expect(text).to.equal('ScantJS Updated');
		});
	});
});
