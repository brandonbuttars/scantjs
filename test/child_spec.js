'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: false});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('$$().childOf()', function () {
		it('childOf works correctly', function* () {
			let el = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var span = $$('span'),
							p = $$('p');
					return span.childOf(p);
				});
			expect(el).to.be.true;
		});

		it('childOf works correctly on grandchild', function* () {
			let el = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var em = $$('em'),
							p = $$('p');
					return em.childOf(p);
				});
			expect(el).to.be.true;
		});
	});
});
