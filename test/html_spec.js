'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: false});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('$$().html', function () {
		it('should get the correct text', function* () {
			let html = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('h1').html;
				});
			expect(html).to.equal('ScantJS');
		});

		it('should set the text correctly', function* () {
			let html = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var h1 = $$('h1');
					h1.html = 'ScantJS <em>Updated</em>';
					return $$('h1').html;
				});
			expect(html).to.equal('ScantJS <em>Updated</em>');
		});
	});
});
