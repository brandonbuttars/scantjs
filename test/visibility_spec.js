'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: false});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('$$().[hide, show, toggle, remove]', function () {
		it('hide works correctly', function* () {
			let el = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.hide();
					return p.hasAttr('hidden');
				});
			expect(el).to.be.true;
		});

		it('show works correctly', function* () {
			let el = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.hide();
					p.show();
					return p.hasAttr('hidden');
				});
			expect(el).to.not.be.true;
		});

		it('toggle show works correctly', function* () {
			let el = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.toggle();
					return p.hasAttr('hidden');
				});
			expect(el).to.be.true;
		});

		it('toggle hide works correctly', function* () {
			let el = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var strong = $$('strong');
					strong.toggle();
					return strong.hasAttr('hidden');
				});
			expect(el).to.not.be.true;
		});

		it('remove works correctly', function* () {
			let el = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.remove();
					return $$('p');
				});
			expect(el).to.not.exist;
		});
	});
});
