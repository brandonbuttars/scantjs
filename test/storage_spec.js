'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

// Custom Actions
Nightmare.action('set_storage', function(type, done) {
	this.evaluate_now(function(type) {
		window[type].storage_value = type +' data';
	}, done, type);
});

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: true});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('localStorage', function () {
		it('can be set and read', function* () {
			let text = yield nightmare
				.goto(demo_url)
				.set_storage('localStorage')
				.evaluate(function () {
					return LS.getItem('storage_value');
				});
			expect(text).to.equal('localStorage data');
		});
	});

	describe('sessionStorage', function () {
		it('can be set and read', function* () {
			let text = yield nightmare
				.goto(demo_url)
				.set_storage('sessionStorage')
				.evaluate(function () {
					return SS.getItem('storage_value');
				});
			expect(text).to.equal('sessionStorage data');
		});
	});
});
