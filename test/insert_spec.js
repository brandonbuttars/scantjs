'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: false});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('$$().[prepend, append, before, after]', function () {
		it('prepend works correctly', function* () {
			let html = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.prepend('<div class="prepend">Prepend</div>');
					return $$('p div.prepend').html;
				});
			expect(html).to.equal('Prepend');
		});

		it('append works correctly', function* () {
			let html = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.append('<div class="append">Append</div>');
					return $$('p span + div.append').html;
				});
			expect(html).to.equal('Append');
		});

		it('before works correctly', function* () {
			let html = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.before('<div class="before">Before</div>');
					return $$('h1 + div.before').html;
				});
			expect(html).to.equal('Before');
		});

		it('after works correctly', function* () {
			let html = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.after('<div class="after">After</div>');
					return $$('p + div.after').html;
				});
			expect(html).to.equal('After');
		});
	});
});
