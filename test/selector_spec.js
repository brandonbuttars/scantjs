'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: false});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('$$() selector type [default, all, class, id, tag]', function () {
		it('should select one element by default', function* () {
			let element = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('ul');
				});
			expect(element).to.exist;
		});

		it('should select \'all\'', function* () {
			let count = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('li', 'all').length;
				});
			expect(count).to.equal(3);
		});

		it('should select by \'class\'', function* () {
			let count = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('li', 'class').length;
				});
			expect(count).to.equal(3);
		});

		it('should select by \'id\'', function* () {
			let text = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('three', 'id').text;
				});
			expect(text).to.equal('List Item Three');
		});

		it('should select by \'tag\'', function* () {
			let count = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('li', 'tag').length;
				});
			expect(count).to.equal(3);
		});
	});
});
