'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: false});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('$$().[add, remove, toggle, has]Class()', function () {
		it('hasClass works correctly', function* () {
			let klass = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('p').hasClass('test-class');
				});
			expect(klass).to.be.true;
		});

		it('addClass works correctly', function* () {
			let klass = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.addClass('add-test-class');
					return p.hasClass('add-test-class');
				});
			expect(klass).to.be.true;
		});

		it('removeClass works correctly', function* () {
			let klass = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.removeClass('test-class');
					return p.hasClass('test-class');
				});
			expect(klass).to.not.be.true;
		});

		it('toggleClass works correctly when class exists', function* () {
			let klass = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.toggleClass('test-class');
					return p.hasClass('test-class');
				});
			expect(klass).to.not.be.true;
		});

		it('toggleClass works correctly when class doesn\'t exist', function* () {
			let klass = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.toggleClass('test-class-2');
					return p.hasClass('test-class-2');
				});
			expect(klass).to.be.true;
		});
	});
});
