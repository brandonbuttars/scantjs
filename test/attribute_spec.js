'use strict';

let Nightmare = require('nightmare'),
		expect = require('chai').expect,
		demo_url = 'http://localhost:3333/demo.html';

describe('ScantJS:', function () {
	let nightmare;

	beforeEach(function* () {
		nightmare = Nightmare({show: false});
	});

	afterEach(function* () {
		yield nightmare.end();
	});

	describe('$$().[get, set, remove, toggle, has]Attr()', function () {
		it('has active attribute', function* () {
			let attribute = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('p').hasAttr('active');
				});
			expect(attribute).to.be.true;
		});

		it('doesn\'t have whatever attribute', function* () {
			let attribute = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					return $$('p').hasAttr('whatever');
				});
			expect(attribute).to.not.be.true;
		});

		it('can set whatever attribute', function* () {
			let attribute = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.setAttr('whatever', 'whatever text');
					return p.hasAttr('whatever');
				});
			expect(attribute).to.be.true;
		});

		it('whatever attribute has the correct value', function* () {
			let attribute = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.setAttr('whatever', 'whatever text');
					return p.getAttr('whatever');
				});
			expect(attribute).to.equal('whatever text');
		});

		it('can toggle \'toggle\' attribute on', function* () {
			let attribute = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.toggleAttr('toggle');
					return p.hasAttr('toggle');
				});
			expect(attribute).to.be.true;
		});

		it('can toggle \'active\' attribute off', function* () {
			let attribute = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.toggleAttr('active');
					return p.hasAttr('active');
				});
			expect(attribute).to.not.be.true;
		});

		it('can remove \'active\' attribute', function* () {
			let attribute = yield nightmare
				.goto(demo_url)
				.evaluate(function () {
					var p = $$('p');
					p.removeAttr('active');
					return p.hasAttr('active');
				});
			expect(attribute).to.not.be.true;
		});
	});
});
