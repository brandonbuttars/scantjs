//////////////////////////////////////////
// DOM Traversal Scripts
// Query Function for Reuse
function $$ (selector, type, parent) {
	'use strict';
	parent = parent || document;

	switch (type) {
		case 'all':	return parent.querySelectorAll(selector);
		case 'class':	return parent.getElementsByClassName(selector);
		case 'id': return parent.getElementById(selector);
		case 'tag':	return parent.getElementsByTagName(selector);
		default: return parent.querySelector(selector);
	}
}

//////////////////////////////////////////
// DOM MANIPULATION
(function (el, doc) {
	'use strict';
	// EXTEND NATIVE ELEMENTS
	// Event Dispatcher
	function dispatch (element, event, data) {
		// Simplified Firing an Event
		var evts = event.replace(/\s+/g, '').split(','),
				length = evts.length;

		element.dispatchEvent(new CustomEvent(event, data));
		if (length > 1) {
			for (var i = 0; i < length; i++) {
				element.dispatchEvent(new CustomEvent(evts[i], data));
			}
		} else {
			element.dispatchEvent(new CustomEvent(evts[0], data));
		}
		return element;
	}

	// Event Listener
	function listen (element, event, fnc, use_capture) {
		var evts = event.replace(/\s+/g, '').split(','),
				length = evts.length;

		use_capture = use_capture || false;

		if (length > 1) {
			for (var i = 0; i < length; i++) {
				element.addEventListener(evts[i], fnc, use_capture);
			}
		} else {
			element.addEventListener(evts[0], fnc, use_capture);
		}
	}

	// HTMLElement
	// scantJS Query Method
	el.prototype.$$ = function (selector, type) {
		return $$(selector, type, this);
	};

	el.prototype.fire = function (event, data) {
		dispatch(this, event, data);
	};

	el.prototype.on = function (event, fnc, use_capture) {
		listen(this, event, fnc, use_capture);
	};

	// Attribute Methods
	el.prototype.getAttr = function (attr) {
		return this.getAttribute(attr);
	};

	el.prototype.setAttr = function (attr, val) {
		this.setAttribute(attr, val || ''); return this;
	};

	el.prototype.removeAttr = function (attr) {
		if (this.getAttr(attr) !== null) {
			this.removeAttribute(attr); return this;
		}
	};

	el.prototype.toggleAttr = function (attr, val) {
		if (this.getAttr(attr) !== null) {
			this.removeAttr(attr);
		} else {
			this.setAttr(attr, val || '');
		}
		return this;
	};

	el.prototype.hasAttr = function (attr) {
		return this.getAttr(attr) !== null ? true : false;
	};

	// Class Methods
	el.prototype.addClass = function (klass) {
		this.classList.add(klass); return this;
	};

	el.prototype.removeClass = function (klass) {
		this.classList.remove(klass); return this;
	};

	el.prototype.toggleClass = function (klass) {
		this.classList.toggle(klass); return this;
	};

	el.prototype.hasClass = function (klass) {
		return this.classList.contains(klass);
	};

	Object.defineProperty(el.prototype, 'html', {
		get: function () {
			return this.innerHTML;
		},
		set: function (val) {
			this.innerHTML = val;
		}
	});

	Object.defineProperty(el.prototype, 'text', {
		get: function () {
			return this.innerText;
		},
		set: function (val) {
			this.innerText = val;
		}
	});

	// Insert HTML/Content
	el.prototype.prepend = function (html) {
		this.insertAdjacentHTML('afterbegin', html);
	};

	el.prototype.append = function (html) {
		this.insertAdjacentHTML('beforeend', html);
	};

	el.prototype.before = function (html) {
		this.insertAdjacentHTML('beforebegin', html);
	};

	el.prototype.after = function (html) {
		this.insertAdjacentHTML('afterend', html);
	};

	// Visibility
	el.prototype.hide = function () {
		this.setAttr('hidden');
	};

	el.prototype.show = function () {
		this.removeAttr('hidden');
	};

	el.prototype.toggle = function () {
		this.toggleAttr('hidden');
	};

	el.prototype.remove = function () {
		this.parentNode.removeChild(this);
	};

	// Genealogy
	el.prototype.childOf = function (parent) {
		var node = this.parentNode;
		while (node !== null) {
			if (node === parent) { return true; }
			node = node.parentNode;
		}
		return false;
	};

	// Debugging Helpers
	el.prototype.info = function () {
		console.info(this);
	};

	el.prototype.log = function () {
		console.log(this);
	};

	// HTMLDocument
	// scantJS Query Method
	doc.prototype.$$ = function (selector, type) {
		return $$(selector, type, document);
	};

	doc.prototype.fire = function (event, data) {
		dispatch(document, event, data);
	};

	doc.prototype.on = function (event, fnc, use_capture) {
		listen(document, event, fnc, use_capture);
	};
} (HTMLElement, HTMLDocument));

//////////////////////////////////////////
// PUB SUB
// https://davidwalsh.name/pubsub-javascript
function PubSubClass() {
	'use strict';
	var topics = {},
  		hop = topics.hasOwnProperty;

  return {
    subscribe: function(topic, listener) {
      // Create the topic's object if not yet created
      if (!hop.call(topics, topic)) { topics[topic] = []; }

      // Add the listener to queue
      var index = topics[topic].push(listener) -1;

      // Provide handle back for removal of topic
      return {
        remove: function() {
          delete topics[topic][index];
        }
      };
    },
    publish: function(topic, info) {
      // If the topic doesn't exist, or there's no listeners in queue, just leave
      if (!hop.call(topics, topic)) { return; }

      // Cycle through topics queue, fire!
      topics[topic].forEach(function(item) {
      	item(info !== undefined ? info : {});
      });
    }
  };
}

var Station = new PubSubClass();

//////////////////////////////////////////
// STORAGE IMPROVEMENTS
(function (s) {
  'use strict';
  // Manage Session Objects
  s.prototype.setObject = function (key, obj) {
    this.setItem(key, JSON.stringify(obj));
  };

  s.prototype.getObject = function (key) {
    if (this.getItem(key)) {
      return JSON.parse(this.getItem(key));
    }
  };

  s.prototype.getObjectItem = function (key, item) {
    var obj = this.getObject(key);
    if (obj) {
      return obj[item];
    }
  };

  s.prototype.updateObject = function (key, item, value) {
    var obj = this.getObject(key),
        val = value || false;

    if (obj) {
      if (val) {
        obj[item] = val;
      } else {
        delete obj[item];
      }
      this.setObject(key, obj);
    }
  };
} (window.Storage));

// SHARED FUNCTIONS
function setRemoveItem(obj, label) {
  'use strict';
  obj.setItem = function (e) {
    document.fire(label +'-updated, '+ label +'-set, '+ label +'-'+ e +'-updated', this);
    obj.setItem.apply(this, arguments);
  };

  // Add event when localStorage is updated/removed.
  obj.removeItem = function (e) {
    document.fire(label +'-updated, '+ label +'-removed, '+ label +'-'+ e +'-updated', this);
    obj.removeItem.apply(this, arguments);
  };
}

//////////////////////////////////////////
// LOCALSTORAGE
(function (ls) {
  'use strict';
	setRemoveItem(ls, 'storage');
} (window.localStorage));

//////////////////////////////////////////
// SESSIONSTORAGE
(function (ss) {
  'use strict';
	setRemoveItem(ss, 'session');
} (window.sessionStorage));

// Set global variables for the different storage types
var LS = window.localStorage,
    SS = window.sessionStorage;
