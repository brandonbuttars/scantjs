//////////////////////////////////////////
// DOM Traversal Scripts
// Query Function for Reuse
function $$ (selector, type, parent) {
	'use strict';
	parent = parent || document;

	switch (type) {
		case 'all':	return parent.querySelectorAll(selector);
		case 'class':	return parent.getElementsByClassName(selector);
		case 'id': return parent.getElementById(selector);
		case 'tag':	return parent.getElementsByTagName(selector);
		default: return parent.querySelector(selector);
	}
}
