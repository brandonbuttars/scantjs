//////////////////////////////////////////
// DOM MANIPULATION
(function (el, doc) {
	'use strict';
	// EXTEND NATIVE ELEMENTS
	// Event Dispatcher
	function dispatch (element, event, data) {
		// Simplified Firing an Event
		var evts = event.replace(/\s+/g, '').split(','),
				length = evts.length;

		element.dispatchEvent(new CustomEvent(event, data));
		if (length > 1) {
			for (var i = 0; i < length; i++) {
				element.dispatchEvent(new CustomEvent(evts[i], data));
			}
		} else {
			element.dispatchEvent(new CustomEvent(evts[0], data));
		}
		return element;
	}

	// Event Listener
	function listen (element, event, fnc, use_capture) {
		var evts = event.replace(/\s+/g, '').split(','),
				length = evts.length;

		use_capture = use_capture || false;

		if (length > 1) {
			for (var i = 0; i < length; i++) {
				element.addEventListener(evts[i], fnc, use_capture);
			}
		} else {
			element.addEventListener(evts[0], fnc, use_capture);
		}
	}

	// HTMLElement
	// scantJS Query Method
	el.prototype.$$ = function (selector, type) {
		return $$(selector, type, this);
	};

	el.prototype.fire = function (event, data) {
		dispatch(this, event, data);
	};

	el.prototype.on = function (event, fnc, use_capture) {
		listen(this, event, fnc, use_capture);
	};

	// Attribute Methods
	el.prototype.getAttr = function (attr) {
		return this.getAttribute(attr);
	};

	el.prototype.setAttr = function (attr, val) {
		this.setAttribute(attr, val || ''); return this;
	};

	el.prototype.removeAttr = function (attr) {
		if (this.getAttr(attr) !== null) {
			this.removeAttribute(attr); return this;
		}
	};

	el.prototype.toggleAttr = function (attr, val) {
		if (this.getAttr(attr) !== null) {
			this.removeAttr(attr);
		} else {
			this.setAttr(attr, val || '');
		}
		return this;
	};

	el.prototype.hasAttr = function (attr) {
		return this.getAttr(attr) !== null ? true : false;
	};

	// Class Methods
	el.prototype.addClass = function (klass) {
		this.classList.add(klass); return this;
	};

	el.prototype.removeClass = function (klass) {
		this.classList.remove(klass); return this;
	};

	el.prototype.toggleClass = function (klass) {
		this.classList.toggle(klass); return this;
	};

	el.prototype.hasClass = function (klass) {
		return this.classList.contains(klass);
	};

	Object.defineProperty(el.prototype, 'html', {
		get: function () {
			return this.innerHTML;
		},
		set: function (val) {
			this.innerHTML = val;
		}
	});

	Object.defineProperty(el.prototype, 'text', {
		get: function () {
			return this.innerText;
		},
		set: function (val) {
			this.innerText = val;
		}
	});

	// Insert HTML/Content
	el.prototype.prepend = function (html) {
		this.insertAdjacentHTML('afterbegin', html);
	};

	el.prototype.append = function (html) {
		this.insertAdjacentHTML('beforeend', html);
	};

	el.prototype.before = function (html) {
		this.insertAdjacentHTML('beforebegin', html);
	};

	el.prototype.after = function (html) {
		this.insertAdjacentHTML('afterend', html);
	};

	// Visibility
	el.prototype.hide = function () {
		this.setAttr('hidden');
	};

	el.prototype.show = function () {
		this.removeAttr('hidden');
	};

	el.prototype.toggle = function () {
		this.toggleAttr('hidden');
	};

	el.prototype.remove = function () {
		this.parentNode.removeChild(this);
	};

	// Genealogy
	el.prototype.childOf = function (parent) {
		var node = this.parentNode;
		while (node !== null) {
			if (node === parent) { return true; }
			node = node.parentNode;
		}
		return false;
	};

	// Debugging Helpers
	el.prototype.info = function () {
		console.info(this);
	};

	el.prototype.log = function () {
		console.log(this);
	};

	// HTMLDocument
	// scantJS Query Method
	doc.prototype.$$ = function (selector, type) {
		return $$(selector, type, document);
	};

	doc.prototype.fire = function (event, data) {
		dispatch(document, event, data);
	};

	doc.prototype.on = function (event, fnc, use_capture) {
		listen(document, event, fnc, use_capture);
	};
} (HTMLElement, HTMLDocument));
