//////////////////////////////////////////
// STORAGE IMPROVEMENTS
(function (s) {
  'use strict';
  // Manage Session Objects
  s.prototype.setObject = function (key, obj) {
    this.setItem(key, JSON.stringify(obj));
  };

  s.prototype.getObject = function (key) {
    if (this.getItem(key)) {
      return JSON.parse(this.getItem(key));
    }
  };

  s.prototype.getObjectItem = function (key, item) {
    var obj = this.getObject(key);
    if (obj) {
      return obj[item];
    }
  };

  s.prototype.updateObject = function (key, item, value) {
    var obj = this.getObject(key),
        val = value || false;

    if (obj) {
      if (val) {
        obj[item] = val;
      } else {
        delete obj[item];
      }
      this.setObject(key, obj);
    }
  };
} (window.Storage));

// SHARED FUNCTIONS
function setRemoveItem(obj, label) {
  'use strict';
  obj.setItem = function (e) {
    document.fire(label +'-updated, '+ label +'-set, '+ label +'-'+ e +'-updated', this);
    obj.setItem.apply(this, arguments);
  };

  // Add event when localStorage is updated/removed.
  obj.removeItem = function (e) {
    document.fire(label +'-updated, '+ label +'-removed, '+ label +'-'+ e +'-updated', this);
    obj.removeItem.apply(this, arguments);
  };
}

//////////////////////////////////////////
// LOCALSTORAGE
(function (ls) {
  'use strict';
	setRemoveItem(ls, 'storage');
} (window.localStorage));

//////////////////////////////////////////
// SESSIONSTORAGE
(function (ss) {
  'use strict';
	setRemoveItem(ss, 'session');
} (window.sessionStorage));

// Set global variables for the different storage types
var LS = window.localStorage,
    SS = window.sessionStorage;
