module.exports = function(grunt) {
	grunt.initConfig({
		concat: {
			dist: {
				src: [
					'src/scant-selector.js',
					'src/scant-dom.js',
					'src/scant-pubsub.js',
					'src/scant-storage.js'
				],
				dest: 'dist/scant.js'
			}
		},
		jshint: {
			options: {
				"curly": true,
				"eqeqeq": true,
				"strict": true,
				"undef": false,
				"unused": false,
				"globals": {
					"window": false,
					"document": false,
					"$$": false
				}
			},
			dist: ['src/**/*.js']
		},
		uglify: {
			options: {
				mangle: false
			},
			dist: {
				files: {
					'dist/scant.min.js': ['dist/scant.js']
				}
			}
		},
		watch: {
			scripts: {
				files: ['src/**/*.js'],
				tasks: ['jshint', 'concat', 'uglify'],
				options: {
					spawn: false
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['jshint', 'concat', 'uglify']);
	grunt.registerTask('build', ['jshint', 'concat', 'uglify', 'watch']);
};
